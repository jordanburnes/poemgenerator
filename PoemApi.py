import json
import glob
import random
from flask import Flask

#WEB STUFF
app = Flask(__name__)

@app.route('/')
def index():
    return 'Hello World'


#WEB STUFF

### Gets a random poem by entered regoin. Enter "none" if you want a random one
@app.route('/GetPoem/<region>')
def GetRandomPoem(region) :
    tempString = ""
    if region == 'none' :
        return ChooseRandom(poems)
    else :
        temp = []
        for item in poems:
            if region in item["region"] :
                temp.append(item)
        poemObject = ChooseRandom(temp)
        for words in poemObject["text"]:
            tempString += words + '\n'
        return tempString

def ChooseRandom(list) :
    return(random.choice(list))

###grabs three random poems from list and returns them as one string seperated by new lines
def GrabThree(list):
    listOfThree = []
    text = ""
    while len(listOfThree) != 3 :
       temp = random.choice(list)
       if temp not in listOfThree:
           listOfThree.append(temp)
    for item in listOfThree:
        text += item + '\n'
    return text

###Gets all poems by region. returns list of strings
def GetAllByRegion(region):
    listOfPoems = []
    if region == 'none':
        for item in poems:
            tempString = ""
            for words in item["text"]:
                tempString += words + '\n'
            listOfPoems.append(tempString)
    else:
        for item in poems:
            if region in item["region"]:
                tempString = ""
                for words in item["text"]:
                    tempString += words + '\n'
                listOfPoems.append(tempString)
    return listOfPoems


from collections import Counter
poems = []
regions = []
distinctRegions = []

#populates poems list with poems
for filename in glob.glob('*/*/*.json'):
    with open (filename) as json_file:
        data = json.load(json_file)
    poems.append(data)
    regions.append(data["region"])

#creates a list of regions
regions = list(set(regions))


tempList = GetAllByRegion("Asia")
print(GrabThree(tempList))

if __name__ == '__main__':
    app.run(debug = True, host="0.0.0.0")